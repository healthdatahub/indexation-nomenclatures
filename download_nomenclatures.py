import wget
import shutil
import os
import tarfile

def bar_custom(current, total, width=80):

    print("Downloading: %d%% [%d / %d] bytes" % (current / total * 100, current, total))

def dl_from_gitlab(url, tarname, untarname):
	
	if tarname in os.listdir():
	    os.remove(tarname)

	if untarname in os.listdir():
	    shutil.rmtree(untarname)

	wget.download(url, tarname, bar=bar_custom)
	tar = tarfile.open(tarname, "r:gz")
	tar.extractall()
	tar.close()

	print('------Nomenclatures téléchargées------')

def get_filetype_from_tree(folder, extension='.csv'):

	files = []

	for dossier, sous_dossiers, fichiers in os.walk(folder):
	    for fichier in fichiers:
	        if fichier.endswith(extension): 
	            files.append(os.path.join(dossier, fichier))
	return(files)

if __name__=='__main__':
	
    url = 'https://gitlab.com/healthdatahub/schema-snds/-/archive/master/schema-snds-master.tar.gz?path=nomenclatures'
    tarname = 'nomenclatures.tar.gz'
    untarname = 'schema-snds-master-nomenclatures'

    dl_from_gitlab(url, tarname, untarname)
    nomenclatures = get_filetype_from_tree(untarname, extension='.csv')