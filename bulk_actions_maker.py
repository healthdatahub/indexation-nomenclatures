import pandas as pd
from math import isnan

def df_to_doc_generator(df, index_name):
    """Transforme un pd.DataFrame en un générateur de dictionnaires (un dictionnaire
    par ligne du pd.DataFrame)

    Args:
        df (pd.DataFrame): dataframe a ingérer dans ES
        index_name (str): nom de l'index dans ES

    Returns:
        Un générateur qui yield un dictionnaire par ligne du pd.DataFrame dans le
        format pour l'ingestion dans ES via l'API Bulk
    """

    df_dict = df.to_dict(orient='records')

    for row in df_dict:

        # Cette boucle for permet d'enlever les NaN empêchant l'import
        for key in list(row):
            try:
                if isnan(row[key]):
                    row.pop(key, None)
            except TypeError:
                pass

        yield {
            "_op_type": "index",
            "_index": index_name,
            "_source": row
        }
