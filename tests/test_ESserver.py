#!/usr/bin/env python
#-*- coding: utf-8 -*-

from elasticsearch import Elasticsearch

def test_connexion():
    es = Elasticsearch(['http://snds2vec.health-data-hub.fr:9200'], http_auth=('snds_reader', 'DICOsnds'))
    assert es.ping()

def test_nomenclatures():
    es = Elasticsearch(['http://snds2vec.health-data-hub.fr:9200'], http_auth=('snds_reader', 'DICOsnds'))
    test = es.indices.get("nomenclature")
    assert len(test)>100
